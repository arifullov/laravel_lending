<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Page;
use App\Service;
use App\Portfolio;
use App\People;
use Mail;

class IndexController extends Controller
{
    public function execute(Request $request) {
        if ($request->isMethod('post')) {
            $messages = [
              'required' => "Поле :attribute обязательно к заполнению",
              'email' => "Поле :attribute должно соответсвовать email адресу",
            ];
            $this->validate($request, [
               'name' => 'required|max:255',
               'email' => 'required|max:255',
               'text' => 'required'
            ], $messages);

            $data = $request->all();
            $result = Mail::send('site.email', ['data' => $data], function ($message) use ($data) {
                $mailAdmin = env('MAIL_ADMIN');
                $message->from($data["email"], $data['name']);
                $message->to($mailAdmin)->subject('Question');
            });
            if ($result) {
                return redirect()->route('home')->with('status', 'Email is send');
            }
        }


        $pages = Page::all();
        $portfolio = Portfolio::get(['name', 'filter', 'images']);
        $services = Service::all();
        $people = People::all();
        //dd($pages);
        $arMenu = [];
        foreach ($pages as $page) {
            $arMenu[] = [
              'title' => $page->name,
              'alias' => $page->alias,
            ];
        }
        $arMenu[] = [
            'title' => "Services",
            'alias' => "service",
        ];
        $arMenu[] = [
            'title' => "Portfolio",
            'alias' => "Portfolio",
        ];
        $arMenu[] = [
            'title' => "Team",
            'alias' => "team",
        ];
        $arMenu[] = [
            'title' => "Contacts",
            'alias' => "contact",
        ];
        return view('site.index',[
            'menu' => $arMenu,
            'pages' => $pages,
            'services' => $services,
            'portfolio' => $portfolio,
            'people' => $people,
        ]);
    }
}
