<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Page;

class PagesEditController extends Controller
{
    public function execute(Page $page, Request $request) {
        if ($request->isMethod('delete')) {
            $page->delete();
            return redirect('admin')->with('status', 'Страница удалена');
        }
        if($request->isMethod('post')) {
            $input = $request->except('_token');

            $messages = [
                'required' => "Поле :attribute обязательно к заполнению",
                'unique' => "Поле :attribute должно быть уникальным",
            ];
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'alias' => 'required|unique:pages,alias,'.$input['id'].'|max:255',
                'text' => 'required',
            ], $messages);
            if ($validator->fails()) {
                return redirect()->route('pagesEdit', ['page'=> $input['id']])->withErrors($validator)->withInput();
            }
            if ($request->hasFile('images')) {
                $file = $request->file('images');
                $input['images'] = "/load_images/".$file->getClientOriginalName();
                $file->move(public_path(), $input['images']);
            } else {
                $input['images'] = $input['old_images'];
            }
            unset($input['old_images']);
            $page->fill($input);
            if($page->update()) {
                return redirect('admin')->with('status', 'Страница обновлена');
            }
        }
        $pageData = $page->toArray();

        if (view()->exists('admin.pages_edit')) {
            $data = [
              'title' => 'Редактирование страницы - '.$pageData['name'],
              'data' => $pageData,
            ];
            return view('admin.pages_edit', $data);
        } else {
            abort('404');
        }
    }
}
